package main

import (
	"io"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
)

func main() {
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome from client1"))
	})
	r.Post("/client2", func(w http.ResponseWriter, r *http.Request) {
		log.Println("sending request to client 2")

		client := http.Client{
			Timeout: 6 * time.Second,
		}
		resp, err := client.Get("http://cli2:80")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		defer resp.Body.Close()
		io.Copy(w, resp.Body)
	})
	http.ListenAndServe(":80", r)
}
