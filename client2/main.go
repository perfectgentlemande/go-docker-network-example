package main

import (
	"io"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
)

func main() {
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome from client2"))
	})
	r.Post("/client1", func(w http.ResponseWriter, r *http.Request) {
		log.Println("sending request to client 1")

		client := http.Client{
			Timeout: 6 * time.Second,
		}
		resp, err := client.Get("http://cli1:80")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		defer resp.Body.Close()
		io.Copy(w, resp.Body)
	})
	http.ListenAndServe(":80", r)
}
